#!/bin/bash

INVENTORY=~/.rapid_ansible_hosts 
echo localhost ansible_connection=local > $INVENTORY
if ansible-playbook -i $INVENTORY ./install-mate.yaml   && \
   ansible-playbook -i $INVENTORY ./install-chrome.yaml && \
   ansible-playbook -i $INVENTORY ./install-vscode.yaml && \
   ansible-playbook -i $INVENTORY ./install-jdk.yaml 
then
  echo "sucess MATE CHROME  desktop install succeeded" >> /tmp/rapid_image_complete
else
  echo "fail  playbook failed" >> /tmp/rapid_image_complete
fi
