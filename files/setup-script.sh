#!/bin/bash

# INSTALL PWNDBG
cd ~
git clone https://github.com/pwndbg/pwndbg
cd pwndbg
./setup.sh


# INSTALL GHIDRA
cd ~
wget https://github.com/NationalSecurityAgency/ghidra/releases/download/Ghidra_11.0_build/ghidra_11.0_PUBLIC_20231222.zip
unzip ghidra_11.0_PUBLIC_20231222.zip
